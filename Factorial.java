import java.util.Scanner;

public class Factorial
{

  public static void main (String[]args)
  {

    System.out.println ("Please Enter a number to output its Factorial: ");

    Scanner input = new Scanner (System.in);

    int N = Integer.parseInt (input.next ());

    long Fact = 1;


    for (int i = 1; i <= N; i++)
      {

	Fact *= i;		// 1*2*3* ... *N
      }
    System.out.println (N + "! = " + Fact);

  }
}

